/**
 * Module dependencies.
 */
const passport = require('passport');
const crypto = require('crypto');
const util = require('util');
const debug = require('debug');

const debugLog = debug('passport-apikeysecret:');

/**
 * `Strategy` constructor.
 *
 * The api key authentication strategy authenticates requests based on the
 * credentials submitted.
 *
 * Applications must supply a `verify` callback which accepts `key`
 * credentials, and then calls the `done` callback supplying a
 * `user`, which should be set to `false` if the credentials are not valid, and the api secret.
 * If an exception occured, `err` should be set.
 *
 * Optionally, `options` can be used to change the fields in which the
 * credentials are found.
 *
 * Options:
 *   - `apiKeyField`  field name where the api key is found, defaults to _x-access-key_
 *   - `signatureField`  field name where the signature is found, defaults to _x-access-sign_
 *   - `timestampField`  field name where the timestamp is found, defaults to _x-access-timestamp_
 *   - `timestampOffset` value, specified in seconds, which acts as tolerance
 *      for the request timestamp, defaults to 60
 *   - `signatureError` a sync function with signature: (req,secret) -> null | { message: "Problem"}
 *      for checking the signature is valid
 *   - `passReqToCallback`  when `true`, `req` is the first argument to the verify callback
 *      (default: `false`)
 *
 * Examples:
 *
 *     passport.use(new APIKeyStrategy(
 *       function(key, done) {
 *         User.findOne({ api_key: key }, function (err, user) {
 *           done(err, user, secret);
 *         });
 *       }
 *     ));
 *
 * @param {Object} options
 * @param {Function} verify
 * @api public
 */
function Strategy(_options, _verify) {
  const signatureError = (req, secret) => {
    const path = req.originalUrl.toLowerCase();

    const body = JSON.stringify(req.body);

    const method = req.method.toUpperCase();

    const timestamp = req.headers[this.timestampField];

    const signature = req.headers[this.signatureField];

    const apiKey = req.headers[this.keyField];

    let milliseconds;
    try {
      milliseconds = parseInt(timestamp, 10);
    } catch (ex) {
      return { message: `cannot parse timestamp: ${timestamp}` };
    }

    const ourTime = Date.now();
    const tolerance = 1000 * this.timestampOffset; // milliseconds
    if (milliseconds - ourTime < -1 * tolerance || milliseconds - ourTime > tolerance) {
      return { message: `invalid timestamp ${timestamp}` };
    }

    const text = timestamp + method + path + body;
    const key = Buffer.from(secret, 'base64');
    const hmac = crypto.createHmac('sha256', key);

    const ourSign = hmac.update(text).digest('base64');
    if (ourSign !== signature) {
      const parameters = {
        path,
        body: req.body,
        method,
        timestamp,
        apiKey,
        secret: secret.slice(0, 2),
      };

      debugLog(
        `Unexpected signature for: ${JSON.stringify(
          parameters,
        )}. Computed signature is ${ourSign}, but received ${signature} instead from the headers`,
      );
      return { message: 'invalid signature' };
    }
    return null;
  };

  let options = _options;
  let verify = _verify;
  if (typeof options === 'function') {
    verify = options;
    options = {};
  }
  if (!verify) throw new Error('apikeysecret authentication strategy requires a verify function');

  this.keyField = options.apiKeyField || 'x-access-key';
  this.signatureField = options.signatureField || 'x-access-sign';
  this.timestampField = options.timestampField || 'x-access-timestamp';
  this.signatureError = options.signatureError || signatureError;
  this.timestampOffset = options.timestampOffset || 600; // 60 seconds

  passport.Strategy.call(this);
  this.name = 'apikeysecret';
  this.verify = verify;
  this.realm = options.realm || 'Users';
  this.passReqToCallback = options.passReqToCallback;
}

/**
 * Inherit from `passport.Strategy`.
 */
util.inherits(Strategy, passport.Strategy);

/**
 * Authenticate request based on the contents of a json-bodied HTTP request.
 *
 * @param {Object} req
 * @api protected
 */
Strategy.prototype.authenticate = function authenticate(req) {
  const apikey = req.headers[this.keyField];
  const signature = req.headers[this.signatureField];
  const timestamp = req.headers[this.timestampField];

  if (!apikey) {
    return this.fail(this.challenge('missing key'));
  }
  if (!signature) {
    return this.fail(this.challenge('missing signature'));
  }
  if (!timestamp) {
    return this.fail(this.challenge('missing timestamp'));
  }

  const verified = (err, actor, secret, info) => {
    if (err) {
      debugLog(`Received error from verify fn: ${err.message}`);
      return this.error(err);
    }
    if (!actor) {
      debugLog('Verify fn did not provide an actor');
      return this.fail(info);
    }
    if (!secret) {
      debugLog('Verify fn did not provide a secret');
      return this.fail(info);
    }
    const error = this.signatureError(req, secret);
    if (error) {
      debugLog(`Signature verification failed with message: ${error.message}`);
      return this.fail(this.challenge(error.message));
    }

    return this.success(actor, info);
  };

  if (this.passReqToCallback) {
    return this.verify(req, apikey, verified);
  }
  return this.verify(apikey, verified);
};

Strategy.prototype.challenge = function getChallenge(desc) {
  let challenge = `ApiKey realm="${this.realm}"`;
  if (this.scope) {
    challenge += `, scope="${this.scope.join(' ')}"`;
  }
  if (desc && desc.length) {
    challenge += `, error_description="${desc}"`;
  }

  return challenge;
};

/**
 * Expose `Strategy`.
 */
module.exports = Strategy;
