/**
 * `BadRequestError` error.
 *
 * @api public
 */
function BadRequestError(message) {
  Error.call(this);
  // eslint-disable-next-line
  Error.captureStackTrace(this, arguments.callee);
  this.name = 'BadRequestError';
  this.message = message || null;
}

/**
 * Inherit from `Error`.
 */
// eslint-disable-next-line
BadRequestError.prototype.__proto__ = Error.prototype;

/**
 * Expose `BadRequestError`.
 */
module.exports = BadRequestError;
