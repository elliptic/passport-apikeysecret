/**
 * Module dependencies.
 */
const Strategy = require('./strategy');
const BadRequestError = require('./errors/badrequesterror');

/**
 * Framework version.
 */
require('pkginfo')(module, 'version');

/**
 * Expose constructors.
 */
exports.Strategy = Strategy;

exports.BadRequestError = BadRequestError;
