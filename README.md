# passport-apikeysecret

Api key and secret authentication strategy for Passport.

## Headers

By default, this strategy expects the following request headers:
- `x-access-key`: the API key that is trying to authenticate
- `x-access-sign`: the client-computed signature that has to be verified
- `x-access-timestamp`: timestamp of the request that is used to compute the signature

## Debug

Setting the environment variable DEBUG=passport-apikeysecret:* will show a detailed description in case the computed signature doesn't match the one that has been provided.
